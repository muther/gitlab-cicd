FROM python:3.8.10

RUN python3 -m pip install pylint nose2 nose2[coverage_plugin] build

#ENV TRIVY_VERSION 0.52.0
#RUN cd /usr/bin && wget --no-verbose https://github.com/aquasecurity/trivy/releases/download/v${TRIVY_VERSION}/trivy_${TRIVY_VERSION}_Linux-64bit.tar.gz -O - | tar -zxvf -

RUN apt update
RUN apt install -y wget apt-transport-https gnupg lsb-release
RUN wget -qO - https://aquasecurity.github.io/trivy-repo/deb/public.key | apt-key add -
RUN echo deb https://aquasecurity.github.io/trivy-repo/deb $(lsb_release -sc) main | tee -a /etc/apt/sources.list.d/trivy.list
RUN apt update && apt install -y trivy
